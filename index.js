const express = require('express')
const app = express()

let courses = [
    {
        id: 1,
        courseName: "Basic HTML",
        description: "Learn the basics of HTML",
        price: 50000
    },
    {
        id: 2,
        courseName: "Basic CSS",
        description: "Learn the basics of CSS",
        price: 35000
    },
    {
        id: 3,
        courseName: "Basic Javascript",
        description: "Learn the basics of Javascript",
        price: 60000
    },
]

app.get('/api/courses', (req, res) => {
    res.send(courses)
})

app.get('/api/courses/:id', (req, res) => {
    let course = courses.find(course => course.id == req.params.id)
    res.send(course)
})

app.listen(3000,()=>{
    console.log('Listening')
})


